lexer grammar JLexer;  

// Tokens
// Operadores aritméticos
MUL:            '*';
DIV:            '/';
SUM:            '+'; 
RES:            '-';
POWF:           'powf';
POW:            'pow';
MOD:            '%';

// Operadores relacionales
MAY_IGU:        '>=';
MEN_IGU:        '<=';
IGU_IGU:        '==';
DIF:            '!=';
MAY:            '>';
MEN:            '<';

// Operadores lógicos
AND:            '&&';
OR1:            '||';
NOT:            '!';

BRAZO:          '=>';
IGUAL:          '=';
OR2:            '|';
ANY:            '_';

// Simbolos especiales
PARIZQ:         '(';
PARDER:         ')';
LLAIZQ:         '{';
LLADER:         '}';
CORIZQ:         '[';
CORDER:         ']';
PTCOMA:         ';';
DOSPT:          ':';
PUNTO:          '.';
COMA:           ',';
RETORNO:        '->';

// Funciones nativas
AS:             'as';
ABS:            'abs';
SQRT:           'sqrt';
TO_OWNED:       '.to_owned()';
TO_STRING:      '.to_string()';
CLONE:          '.clone()';

// Funciones nativas para vectores
NEW:            'new()';
LEN:            'len()';
PUSH:           'push';
REMOVE:         'remove';
CONTAINS:       'contains';
INSERT:         'insert';
CAPACITY:       'capacity()';
WITH_CAPACITY:  'with_capacity';

// Palabras reservadas
P_I64:          'i64';
P_F64:          'f64';
P_BOOL:         'bool';
P_TRUE:         'true';
P_FALSE:        'false';
P_CHAR:         'char';
P_STRING1:      'String';
P_STRING2:      '&str';
P_VEC1:         'Vec';
P_VEC2:         'vec!';
P_STRUCT:       'struct';

P_LET:          'let';
P_MUT:          'mut';
P_PRINTLN:      'println!';
P_PRINT:        'print!';
P_LOOP:         'loop';
P_IF:           'if';
P_ELSE:         'else';
P_WHILE:        'while';
P_MATCH:        'match';
P_FOR:          'for';
P_IN:           'in';
P_FN:           'fn';
P_REFERENCIA:   '&';

// Sentencias de transferencia
P_BREAK:        'break';
P_CONTINUE:     'continue';
P_RETURN:       'return';

// Modulos
P_MOD:          'mod';
P_PUB:          'pub';

// Datos primitivos
NUMBER: [0-9]+;
DECIMAL: [0-9]+'.'[0-9]+;
STRING: '"'~["]*'"';
ID: ([a-zA-Z_])[a-zA-Z0-9_]*;
// Datos a omitir
COMENTARIOL:    '//'~["]* -> skip;
COMENTARIOM:    '/*'~["|\n]*'*/' -> skip;
WHITESPACE:     [ \\\r\n\t]+ -> skip;
fragment
ESC_SEQ
    :   '\\' ('\\'|'@'|'['|']'|'.'|'#'|'+'|'-'|'!'|':'|' ')
    ;