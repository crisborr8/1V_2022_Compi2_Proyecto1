parser grammar JParser;

options { 
  tokenVocab = JLexer;
}

start returns [ArrayList<int> lista]
  : instrucciones {$lista = $instrucciones.lista;} EOF
;

instrucciones returns [ArrayList<int> lista]
  @init {
    $lista = new ArrayList<int>();
  }
  : instruccion {$lista.add($instruccion.valor);}
;
//---------------------
// GENERAL
//---------------------
instruccion returns [int valor]
  : STRING PTCOMA                                                                                   {$valor = 24;}
;