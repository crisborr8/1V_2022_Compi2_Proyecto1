import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws IOException {

        CharStream charStream = CharStreams.fromFileName("./entrada.txt");
        JLexer lexer = new JLexer(charStream);
        CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
        JParser parser = new JParser(commonTokenStream);

        ParseTree parseTree = parser.start();

        App app = new App();
        app.Arbol(parseTree);

        System.out.println("Finalizado");
    }

    private void Arbol(ParseTree arbol){
        ParseTree child;
        for(int i = 0; i < arbol.getChildCount(); i++){
            System.out.println("********");
            child = arbol.getChild(i);
            if (child.getChildCount() == 0){
                CommonToken token = (CommonToken) child.getPayload();
                if (token.getType() == -1) break;
                if (token.getType() == 1) {
                    System.out.println("Error, " + token.getText() + " no es parte");
                    System.out.println("-> Linea: " + token.getLine());
                    System.out.println("-> Columna: " + token.getCharPositionInLine());
                } else{
                    System.out.println("-> Tipo (numero): " + token.getType());
                    System.out.println("-> Tipo (valor): " + JParser.tokenNames[token.getType()]);
                    System.out.println("-> Linea: " + token.getLine());
                    System.out.println("-> Columna: " + token.getCharPositionInLine());
                    System.out.println("-> Valor: " + token.getText());
                }
            } else{
                System.out.println("-> " + child.getPayload().getClass());
                if(child.getPayload().getClass().equals(JParser.InstruccionContext.class)){
                    System.out.println("es una instruccion");
                }
            }
            Arbol(child);
        }
    }
}
