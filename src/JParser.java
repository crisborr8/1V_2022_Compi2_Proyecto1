// Generated from /media/criss/Archivos/Compiladores 2/Proyecto/Proyecto/src/grammar/JParser.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class JParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		MUL=1, DIV=2, SUM=3, RES=4, POWF=5, POW=6, MOD=7, MAY_IGU=8, MEN_IGU=9, 
		IGU_IGU=10, DIF=11, MAY=12, MEN=13, AND=14, OR1=15, NOT=16, BRAZO=17, 
		IGUAL=18, OR2=19, ANY=20, PARIZQ=21, PARDER=22, LLAIZQ=23, LLADER=24, 
		CORIZQ=25, CORDER=26, PTCOMA=27, DOSPT=28, PUNTO=29, COMA=30, RETORNO=31, 
		AS=32, ABS=33, SQRT=34, TO_OWNED=35, TO_STRING=36, CLONE=37, NEW=38, LEN=39, 
		PUSH=40, REMOVE=41, CONTAINS=42, INSERT=43, CAPACITY=44, WITH_CAPACITY=45, 
		P_I64=46, P_F64=47, P_BOOL=48, P_TRUE=49, P_FALSE=50, P_CHAR=51, P_STRING1=52, 
		P_STRING2=53, P_VEC1=54, P_VEC2=55, P_STRUCT=56, P_LET=57, P_MUT=58, P_PRINTLN=59, 
		P_PRINT=60, P_LOOP=61, P_IF=62, P_ELSE=63, P_WHILE=64, P_MATCH=65, P_FOR=66, 
		P_IN=67, P_FN=68, P_REFERENCIA=69, P_BREAK=70, P_CONTINUE=71, P_RETURN=72, 
		P_MOD=73, P_PUB=74, NUMBER=75, DECIMAL=76, STRING=77, ID=78, COMENTARIOL=79, 
		COMENTARIOM=80, WHITESPACE=81;
	public static final int
		RULE_start = 0, RULE_instrucciones = 1, RULE_instruccion = 2;
	private static String[] makeRuleNames() {
		return new String[] {
			"start", "instrucciones", "instruccion"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'*'", "'/'", "'+'", "'-'", "'powf'", "'pow'", "'%'", "'>='", "'<='", 
			"'=='", "'!='", "'>'", "'<'", "'&&'", "'||'", "'!'", "'=>'", "'='", "'|'", 
			"'_'", "'('", "')'", "'{'", "'}'", "'['", "']'", "';'", "':'", "'.'", 
			"','", "'->'", "'as'", "'abs'", "'sqrt'", "'.to_owned()'", "'.to_string()'", 
			"'.clone()'", "'new()'", "'len()'", "'push'", "'remove'", "'contains'", 
			"'insert'", "'capacity()'", "'with_capacity'", "'i64'", "'f64'", "'bool'", 
			"'true'", "'false'", "'char'", "'String'", "'&str'", "'Vec'", "'vec!'", 
			"'struct'", "'let'", "'mut'", "'println!'", "'print!'", "'loop'", "'if'", 
			"'else'", "'while'", "'match'", "'for'", "'in'", "'fn'", "'&'", "'break'", 
			"'continue'", "'return'", "'mod'", "'pub'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "MUL", "DIV", "SUM", "RES", "POWF", "POW", "MOD", "MAY_IGU", "MEN_IGU", 
			"IGU_IGU", "DIF", "MAY", "MEN", "AND", "OR1", "NOT", "BRAZO", "IGUAL", 
			"OR2", "ANY", "PARIZQ", "PARDER", "LLAIZQ", "LLADER", "CORIZQ", "CORDER", 
			"PTCOMA", "DOSPT", "PUNTO", "COMA", "RETORNO", "AS", "ABS", "SQRT", "TO_OWNED", 
			"TO_STRING", "CLONE", "NEW", "LEN", "PUSH", "REMOVE", "CONTAINS", "INSERT", 
			"CAPACITY", "WITH_CAPACITY", "P_I64", "P_F64", "P_BOOL", "P_TRUE", "P_FALSE", 
			"P_CHAR", "P_STRING1", "P_STRING2", "P_VEC1", "P_VEC2", "P_STRUCT", "P_LET", 
			"P_MUT", "P_PRINTLN", "P_PRINT", "P_LOOP", "P_IF", "P_ELSE", "P_WHILE", 
			"P_MATCH", "P_FOR", "P_IN", "P_FN", "P_REFERENCIA", "P_BREAK", "P_CONTINUE", 
			"P_RETURN", "P_MOD", "P_PUB", "NUMBER", "DECIMAL", "STRING", "ID", "COMENTARIOL", 
			"COMENTARIOM", "WHITESPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "JParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public JParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StartContext extends ParserRuleContext {
		public InstruccionesContext instrucciones() {
			return getRuleContext(InstruccionesContext.class,0);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(6);
			instrucciones();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstruccionesContext extends ParserRuleContext {
		public InstruccionContext instruccion() {
			return getRuleContext(InstruccionContext.class,0);
		}
		public InstruccionesContext instrucciones() {
			return getRuleContext(InstruccionesContext.class,0);
		}
		public TerminalNode EOF() { return getToken(JParser.EOF, 0); }
		public InstruccionesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instrucciones; }
	}

	public final InstruccionesContext instrucciones() throws RecognitionException {
		InstruccionesContext _localctx = new InstruccionesContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_instrucciones);
		try {
			setState(12);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(8);
				instruccion();
				setState(9);
				instrucciones();
				}
				break;
			case EOF:
				enterOuterAlt(_localctx, 2);
				{
				setState(11);
				match(EOF);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstruccionContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(JParser.STRING, 0); }
		public TerminalNode PTCOMA() { return getToken(JParser.PTCOMA, 0); }
		public InstruccionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instruccion; }
	}

	public final InstruccionContext instruccion() throws RecognitionException {
		InstruccionContext _localctx = new InstruccionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_instruccion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(14);
			match(STRING);
			setState(15);
			match(PTCOMA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3S\24\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\3\2\3\2\3\3\3\3\3\3\3\3\5\3\17\n\3\3\4\3\4\3\4\3\4\2\2\5\2"+
		"\4\6\2\2\2\21\2\b\3\2\2\2\4\16\3\2\2\2\6\20\3\2\2\2\b\t\5\4\3\2\t\3\3"+
		"\2\2\2\n\13\5\6\4\2\13\f\5\4\3\2\f\17\3\2\2\2\r\17\7\2\2\3\16\n\3\2\2"+
		"\2\16\r\3\2\2\2\17\5\3\2\2\2\20\21\7O\2\2\21\22\7\35\2\2\22\7\3\2\2\2"+
		"\3\16";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}