// Generated from /media/criss/Archivos/Compiladores 2/Proyecto/Proyecto/src/grammar/JLexer.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class JLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		MUL=1, DIV=2, SUM=3, RES=4, POWF=5, POW=6, MOD=7, MAY_IGU=8, MEN_IGU=9, 
		IGU_IGU=10, DIF=11, MAY=12, MEN=13, AND=14, OR1=15, NOT=16, BRAZO=17, 
		IGUAL=18, OR2=19, ANY=20, PARIZQ=21, PARDER=22, LLAIZQ=23, LLADER=24, 
		CORIZQ=25, CORDER=26, PTCOMA=27, DOSPT=28, PUNTO=29, COMA=30, RETORNO=31, 
		AS=32, ABS=33, SQRT=34, TO_OWNED=35, TO_STRING=36, CLONE=37, NEW=38, LEN=39, 
		PUSH=40, REMOVE=41, CONTAINS=42, INSERT=43, CAPACITY=44, WITH_CAPACITY=45, 
		P_I64=46, P_F64=47, P_BOOL=48, P_TRUE=49, P_FALSE=50, P_CHAR=51, P_STRING1=52, 
		P_STRING2=53, P_VEC1=54, P_VEC2=55, P_STRUCT=56, P_LET=57, P_MUT=58, P_PRINTLN=59, 
		P_PRINT=60, P_LOOP=61, P_IF=62, P_ELSE=63, P_WHILE=64, P_MATCH=65, P_FOR=66, 
		P_IN=67, P_FN=68, P_REFERENCIA=69, P_BREAK=70, P_CONTINUE=71, P_RETURN=72, 
		P_MOD=73, P_PUB=74, NUMBER=75, DECIMAL=76, STRING=77, ID=78, COMENTARIOL=79, 
		COMENTARIOM=80, WHITESPACE=81;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"MUL", "DIV", "SUM", "RES", "POWF", "POW", "MOD", "MAY_IGU", "MEN_IGU", 
			"IGU_IGU", "DIF", "MAY", "MEN", "AND", "OR1", "NOT", "BRAZO", "IGUAL", 
			"OR2", "ANY", "PARIZQ", "PARDER", "LLAIZQ", "LLADER", "CORIZQ", "CORDER", 
			"PTCOMA", "DOSPT", "PUNTO", "COMA", "RETORNO", "AS", "ABS", "SQRT", "TO_OWNED", 
			"TO_STRING", "CLONE", "NEW", "LEN", "PUSH", "REMOVE", "CONTAINS", "INSERT", 
			"CAPACITY", "WITH_CAPACITY", "P_I64", "P_F64", "P_BOOL", "P_TRUE", "P_FALSE", 
			"P_CHAR", "P_STRING1", "P_STRING2", "P_VEC1", "P_VEC2", "P_STRUCT", "P_LET", 
			"P_MUT", "P_PRINTLN", "P_PRINT", "P_LOOP", "P_IF", "P_ELSE", "P_WHILE", 
			"P_MATCH", "P_FOR", "P_IN", "P_FN", "P_REFERENCIA", "P_BREAK", "P_CONTINUE", 
			"P_RETURN", "P_MOD", "P_PUB", "NUMBER", "DECIMAL", "STRING", "ID", "COMENTARIOL", 
			"COMENTARIOM", "WHITESPACE", "ESC_SEQ"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'*'", "'/'", "'+'", "'-'", "'powf'", "'pow'", "'%'", "'>='", "'<='", 
			"'=='", "'!='", "'>'", "'<'", "'&&'", "'||'", "'!'", "'=>'", "'='", "'|'", 
			"'_'", "'('", "')'", "'{'", "'}'", "'['", "']'", "';'", "':'", "'.'", 
			"','", "'->'", "'as'", "'abs'", "'sqrt'", "'.to_owned()'", "'.to_string()'", 
			"'.clone()'", "'new()'", "'len()'", "'push'", "'remove'", "'contains'", 
			"'insert'", "'capacity()'", "'with_capacity'", "'i64'", "'f64'", "'bool'", 
			"'true'", "'false'", "'char'", "'String'", "'&str'", "'Vec'", "'vec!'", 
			"'struct'", "'let'", "'mut'", "'println!'", "'print!'", "'loop'", "'if'", 
			"'else'", "'while'", "'match'", "'for'", "'in'", "'fn'", "'&'", "'break'", 
			"'continue'", "'return'", "'mod'", "'pub'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "MUL", "DIV", "SUM", "RES", "POWF", "POW", "MOD", "MAY_IGU", "MEN_IGU", 
			"IGU_IGU", "DIF", "MAY", "MEN", "AND", "OR1", "NOT", "BRAZO", "IGUAL", 
			"OR2", "ANY", "PARIZQ", "PARDER", "LLAIZQ", "LLADER", "CORIZQ", "CORDER", 
			"PTCOMA", "DOSPT", "PUNTO", "COMA", "RETORNO", "AS", "ABS", "SQRT", "TO_OWNED", 
			"TO_STRING", "CLONE", "NEW", "LEN", "PUSH", "REMOVE", "CONTAINS", "INSERT", 
			"CAPACITY", "WITH_CAPACITY", "P_I64", "P_F64", "P_BOOL", "P_TRUE", "P_FALSE", 
			"P_CHAR", "P_STRING1", "P_STRING2", "P_VEC1", "P_VEC2", "P_STRUCT", "P_LET", 
			"P_MUT", "P_PRINTLN", "P_PRINT", "P_LOOP", "P_IF", "P_ELSE", "P_WHILE", 
			"P_MATCH", "P_FOR", "P_IN", "P_FN", "P_REFERENCIA", "P_BREAK", "P_CONTINUE", 
			"P_RETURN", "P_MOD", "P_PUB", "NUMBER", "DECIMAL", "STRING", "ID", "COMENTARIOL", 
			"COMENTARIOM", "WHITESPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public JLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "JLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2S\u0238\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\3\2\3"+
		"\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b"+
		"\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\16\3\16"+
		"\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\24"+
		"\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33"+
		"\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3 \3!\3!\3!\3\"\3"+
		"\"\3\"\3\"\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3%\3%\3"+
		"%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3&\3&\3&\3\'\3\'\3\'"+
		"\3\'\3\'\3\'\3(\3(\3(\3(\3(\3(\3)\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3*\3+"+
		"\3+\3+\3+\3+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\3-\3-"+
		"\3-\3-\3-\3.\3.\3.\3.\3.\3.\3.\3.\3.\3.\3.\3.\3.\3.\3/\3/\3/\3/\3\60\3"+
		"\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3\62\3\62\3\62\3\63\3"+
		"\63\3\63\3\63\3\63\3\63\3\64\3\64\3\64\3\64\3\64\3\65\3\65\3\65\3\65\3"+
		"\65\3\65\3\65\3\66\3\66\3\66\3\66\3\66\3\67\3\67\3\67\3\67\38\38\38\3"+
		"8\38\39\39\39\39\39\39\39\3:\3:\3:\3:\3;\3;\3;\3;\3<\3<\3<\3<\3<\3<\3"+
		"<\3<\3<\3=\3=\3=\3=\3=\3=\3=\3>\3>\3>\3>\3>\3?\3?\3?\3@\3@\3@\3@\3@\3"+
		"A\3A\3A\3A\3A\3A\3B\3B\3B\3B\3B\3B\3C\3C\3C\3C\3D\3D\3D\3E\3E\3E\3F\3"+
		"F\3G\3G\3G\3G\3G\3G\3H\3H\3H\3H\3H\3H\3H\3H\3H\3I\3I\3I\3I\3I\3I\3I\3"+
		"J\3J\3J\3J\3K\3K\3K\3K\3L\6L\u01f7\nL\rL\16L\u01f8\3M\6M\u01fc\nM\rM\16"+
		"M\u01fd\3M\3M\6M\u0202\nM\rM\16M\u0203\3N\3N\7N\u0208\nN\fN\16N\u020b"+
		"\13N\3N\3N\3O\3O\7O\u0211\nO\fO\16O\u0214\13O\3P\3P\3P\3P\7P\u021a\nP"+
		"\fP\16P\u021d\13P\3P\3P\3Q\3Q\3Q\3Q\7Q\u0225\nQ\fQ\16Q\u0228\13Q\3Q\3"+
		"Q\3Q\3Q\3Q\3R\6R\u0230\nR\rR\16R\u0231\3R\3R\3S\3S\3S\2\2T\3\3\5\4\7\5"+
		"\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23"+
		"%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G"+
		"%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\67m8o9q:s;u<w=y>{"+
		"?}@\177A\u0081B\u0083C\u0085D\u0087E\u0089F\u008bG\u008dH\u008fI\u0091"+
		"J\u0093K\u0095L\u0097M\u0099N\u009bO\u009dP\u009fQ\u00a1R\u00a3S\u00a5"+
		"\2\3\2\t\3\2\62;\3\2$$\5\2C\\aac|\6\2\62;C\\aac|\5\2\f\f$$~~\6\2\13\f"+
		"\17\17\"\"^^\t\2\"#%%--/\60<<BB]_\2\u023e\2\3\3\2\2\2\2\5\3\2\2\2\2\7"+
		"\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2"+
		"\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2"+
		"\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2"+
		"\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2"+
		"\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2"+
		"\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M"+
		"\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2"+
		"\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2"+
		"\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s"+
		"\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\2\177"+
		"\3\2\2\2\2\u0081\3\2\2\2\2\u0083\3\2\2\2\2\u0085\3\2\2\2\2\u0087\3\2\2"+
		"\2\2\u0089\3\2\2\2\2\u008b\3\2\2\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091"+
		"\3\2\2\2\2\u0093\3\2\2\2\2\u0095\3\2\2\2\2\u0097\3\2\2\2\2\u0099\3\2\2"+
		"\2\2\u009b\3\2\2\2\2\u009d\3\2\2\2\2\u009f\3\2\2\2\2\u00a1\3\2\2\2\2\u00a3"+
		"\3\2\2\2\3\u00a7\3\2\2\2\5\u00a9\3\2\2\2\7\u00ab\3\2\2\2\t\u00ad\3\2\2"+
		"\2\13\u00af\3\2\2\2\r\u00b4\3\2\2\2\17\u00b8\3\2\2\2\21\u00ba\3\2\2\2"+
		"\23\u00bd\3\2\2\2\25\u00c0\3\2\2\2\27\u00c3\3\2\2\2\31\u00c6\3\2\2\2\33"+
		"\u00c8\3\2\2\2\35\u00ca\3\2\2\2\37\u00cd\3\2\2\2!\u00d0\3\2\2\2#\u00d2"+
		"\3\2\2\2%\u00d5\3\2\2\2\'\u00d7\3\2\2\2)\u00d9\3\2\2\2+\u00db\3\2\2\2"+
		"-\u00dd\3\2\2\2/\u00df\3\2\2\2\61\u00e1\3\2\2\2\63\u00e3\3\2\2\2\65\u00e5"+
		"\3\2\2\2\67\u00e7\3\2\2\29\u00e9\3\2\2\2;\u00eb\3\2\2\2=\u00ed\3\2\2\2"+
		"?\u00ef\3\2\2\2A\u00f2\3\2\2\2C\u00f5\3\2\2\2E\u00f9\3\2\2\2G\u00fe\3"+
		"\2\2\2I\u010a\3\2\2\2K\u0117\3\2\2\2M\u0120\3\2\2\2O\u0126\3\2\2\2Q\u012c"+
		"\3\2\2\2S\u0131\3\2\2\2U\u0138\3\2\2\2W\u0141\3\2\2\2Y\u0148\3\2\2\2["+
		"\u0153\3\2\2\2]\u0161\3\2\2\2_\u0165\3\2\2\2a\u0169\3\2\2\2c\u016e\3\2"+
		"\2\2e\u0173\3\2\2\2g\u0179\3\2\2\2i\u017e\3\2\2\2k\u0185\3\2\2\2m\u018a"+
		"\3\2\2\2o\u018e\3\2\2\2q\u0193\3\2\2\2s\u019a\3\2\2\2u\u019e\3\2\2\2w"+
		"\u01a2\3\2\2\2y\u01ab\3\2\2\2{\u01b2\3\2\2\2}\u01b7\3\2\2\2\177\u01ba"+
		"\3\2\2\2\u0081\u01bf\3\2\2\2\u0083\u01c5\3\2\2\2\u0085\u01cb\3\2\2\2\u0087"+
		"\u01cf\3\2\2\2\u0089\u01d2\3\2\2\2\u008b\u01d5\3\2\2\2\u008d\u01d7\3\2"+
		"\2\2\u008f\u01dd\3\2\2\2\u0091\u01e6\3\2\2\2\u0093\u01ed\3\2\2\2\u0095"+
		"\u01f1\3\2\2\2\u0097\u01f6\3\2\2\2\u0099\u01fb\3\2\2\2\u009b\u0205\3\2"+
		"\2\2\u009d\u020e\3\2\2\2\u009f\u0215\3\2\2\2\u00a1\u0220\3\2\2\2\u00a3"+
		"\u022f\3\2\2\2\u00a5\u0235\3\2\2\2\u00a7\u00a8\7,\2\2\u00a8\4\3\2\2\2"+
		"\u00a9\u00aa\7\61\2\2\u00aa\6\3\2\2\2\u00ab\u00ac\7-\2\2\u00ac\b\3\2\2"+
		"\2\u00ad\u00ae\7/\2\2\u00ae\n\3\2\2\2\u00af\u00b0\7r\2\2\u00b0\u00b1\7"+
		"q\2\2\u00b1\u00b2\7y\2\2\u00b2\u00b3\7h\2\2\u00b3\f\3\2\2\2\u00b4\u00b5"+
		"\7r\2\2\u00b5\u00b6\7q\2\2\u00b6\u00b7\7y\2\2\u00b7\16\3\2\2\2\u00b8\u00b9"+
		"\7\'\2\2\u00b9\20\3\2\2\2\u00ba\u00bb\7@\2\2\u00bb\u00bc\7?\2\2\u00bc"+
		"\22\3\2\2\2\u00bd\u00be\7>\2\2\u00be\u00bf\7?\2\2\u00bf\24\3\2\2\2\u00c0"+
		"\u00c1\7?\2\2\u00c1\u00c2\7?\2\2\u00c2\26\3\2\2\2\u00c3\u00c4\7#\2\2\u00c4"+
		"\u00c5\7?\2\2\u00c5\30\3\2\2\2\u00c6\u00c7\7@\2\2\u00c7\32\3\2\2\2\u00c8"+
		"\u00c9\7>\2\2\u00c9\34\3\2\2\2\u00ca\u00cb\7(\2\2\u00cb\u00cc\7(\2\2\u00cc"+
		"\36\3\2\2\2\u00cd\u00ce\7~\2\2\u00ce\u00cf\7~\2\2\u00cf \3\2\2\2\u00d0"+
		"\u00d1\7#\2\2\u00d1\"\3\2\2\2\u00d2\u00d3\7?\2\2\u00d3\u00d4\7@\2\2\u00d4"+
		"$\3\2\2\2\u00d5\u00d6\7?\2\2\u00d6&\3\2\2\2\u00d7\u00d8\7~\2\2\u00d8("+
		"\3\2\2\2\u00d9\u00da\7a\2\2\u00da*\3\2\2\2\u00db\u00dc\7*\2\2\u00dc,\3"+
		"\2\2\2\u00dd\u00de\7+\2\2\u00de.\3\2\2\2\u00df\u00e0\7}\2\2\u00e0\60\3"+
		"\2\2\2\u00e1\u00e2\7\177\2\2\u00e2\62\3\2\2\2\u00e3\u00e4\7]\2\2\u00e4"+
		"\64\3\2\2\2\u00e5\u00e6\7_\2\2\u00e6\66\3\2\2\2\u00e7\u00e8\7=\2\2\u00e8"+
		"8\3\2\2\2\u00e9\u00ea\7<\2\2\u00ea:\3\2\2\2\u00eb\u00ec\7\60\2\2\u00ec"+
		"<\3\2\2\2\u00ed\u00ee\7.\2\2\u00ee>\3\2\2\2\u00ef\u00f0\7/\2\2\u00f0\u00f1"+
		"\7@\2\2\u00f1@\3\2\2\2\u00f2\u00f3\7c\2\2\u00f3\u00f4\7u\2\2\u00f4B\3"+
		"\2\2\2\u00f5\u00f6\7c\2\2\u00f6\u00f7\7d\2\2\u00f7\u00f8\7u\2\2\u00f8"+
		"D\3\2\2\2\u00f9\u00fa\7u\2\2\u00fa\u00fb\7s\2\2\u00fb\u00fc\7t\2\2\u00fc"+
		"\u00fd\7v\2\2\u00fdF\3\2\2\2\u00fe\u00ff\7\60\2\2\u00ff\u0100\7v\2\2\u0100"+
		"\u0101\7q\2\2\u0101\u0102\7a\2\2\u0102\u0103\7q\2\2\u0103\u0104\7y\2\2"+
		"\u0104\u0105\7p\2\2\u0105\u0106\7g\2\2\u0106\u0107\7f\2\2\u0107\u0108"+
		"\7*\2\2\u0108\u0109\7+\2\2\u0109H\3\2\2\2\u010a\u010b\7\60\2\2\u010b\u010c"+
		"\7v\2\2\u010c\u010d\7q\2\2\u010d\u010e\7a\2\2\u010e\u010f\7u\2\2\u010f"+
		"\u0110\7v\2\2\u0110\u0111\7t\2\2\u0111\u0112\7k\2\2\u0112\u0113\7p\2\2"+
		"\u0113\u0114\7i\2\2\u0114\u0115\7*\2\2\u0115\u0116\7+\2\2\u0116J\3\2\2"+
		"\2\u0117\u0118\7\60\2\2\u0118\u0119\7e\2\2\u0119\u011a\7n\2\2\u011a\u011b"+
		"\7q\2\2\u011b\u011c\7p\2\2\u011c\u011d\7g\2\2\u011d\u011e\7*\2\2\u011e"+
		"\u011f\7+\2\2\u011fL\3\2\2\2\u0120\u0121\7p\2\2\u0121\u0122\7g\2\2\u0122"+
		"\u0123\7y\2\2\u0123\u0124\7*\2\2\u0124\u0125\7+\2\2\u0125N\3\2\2\2\u0126"+
		"\u0127\7n\2\2\u0127\u0128\7g\2\2\u0128\u0129\7p\2\2\u0129\u012a\7*\2\2"+
		"\u012a\u012b\7+\2\2\u012bP\3\2\2\2\u012c\u012d\7r\2\2\u012d\u012e\7w\2"+
		"\2\u012e\u012f\7u\2\2\u012f\u0130\7j\2\2\u0130R\3\2\2\2\u0131\u0132\7"+
		"t\2\2\u0132\u0133\7g\2\2\u0133\u0134\7o\2\2\u0134\u0135\7q\2\2\u0135\u0136"+
		"\7x\2\2\u0136\u0137\7g\2\2\u0137T\3\2\2\2\u0138\u0139\7e\2\2\u0139\u013a"+
		"\7q\2\2\u013a\u013b\7p\2\2\u013b\u013c\7v\2\2\u013c\u013d\7c\2\2\u013d"+
		"\u013e\7k\2\2\u013e\u013f\7p\2\2\u013f\u0140\7u\2\2\u0140V\3\2\2\2\u0141"+
		"\u0142\7k\2\2\u0142\u0143\7p\2\2\u0143\u0144\7u\2\2\u0144\u0145\7g\2\2"+
		"\u0145\u0146\7t\2\2\u0146\u0147\7v\2\2\u0147X\3\2\2\2\u0148\u0149\7e\2"+
		"\2\u0149\u014a\7c\2\2\u014a\u014b\7r\2\2\u014b\u014c\7c\2\2\u014c\u014d"+
		"\7e\2\2\u014d\u014e\7k\2\2\u014e\u014f\7v\2\2\u014f\u0150\7{\2\2\u0150"+
		"\u0151\7*\2\2\u0151\u0152\7+\2\2\u0152Z\3\2\2\2\u0153\u0154\7y\2\2\u0154"+
		"\u0155\7k\2\2\u0155\u0156\7v\2\2\u0156\u0157\7j\2\2\u0157\u0158\7a\2\2"+
		"\u0158\u0159\7e\2\2\u0159\u015a\7c\2\2\u015a\u015b\7r\2\2\u015b\u015c"+
		"\7c\2\2\u015c\u015d\7e\2\2\u015d\u015e\7k\2\2\u015e\u015f\7v\2\2\u015f"+
		"\u0160\7{\2\2\u0160\\\3\2\2\2\u0161\u0162\7k\2\2\u0162\u0163\78\2\2\u0163"+
		"\u0164\7\66\2\2\u0164^\3\2\2\2\u0165\u0166\7h\2\2\u0166\u0167\78\2\2\u0167"+
		"\u0168\7\66\2\2\u0168`\3\2\2\2\u0169\u016a\7d\2\2\u016a\u016b\7q\2\2\u016b"+
		"\u016c\7q\2\2\u016c\u016d\7n\2\2\u016db\3\2\2\2\u016e\u016f\7v\2\2\u016f"+
		"\u0170\7t\2\2\u0170\u0171\7w\2\2\u0171\u0172\7g\2\2\u0172d\3\2\2\2\u0173"+
		"\u0174\7h\2\2\u0174\u0175\7c\2\2\u0175\u0176\7n\2\2\u0176\u0177\7u\2\2"+
		"\u0177\u0178\7g\2\2\u0178f\3\2\2\2\u0179\u017a\7e\2\2\u017a\u017b\7j\2"+
		"\2\u017b\u017c\7c\2\2\u017c\u017d\7t\2\2\u017dh\3\2\2\2\u017e\u017f\7"+
		"U\2\2\u017f\u0180\7v\2\2\u0180\u0181\7t\2\2\u0181\u0182\7k\2\2\u0182\u0183"+
		"\7p\2\2\u0183\u0184\7i\2\2\u0184j\3\2\2\2\u0185\u0186\7(\2\2\u0186\u0187"+
		"\7u\2\2\u0187\u0188\7v\2\2\u0188\u0189\7t\2\2\u0189l\3\2\2\2\u018a\u018b"+
		"\7X\2\2\u018b\u018c\7g\2\2\u018c\u018d\7e\2\2\u018dn\3\2\2\2\u018e\u018f"+
		"\7x\2\2\u018f\u0190\7g\2\2\u0190\u0191\7e\2\2\u0191\u0192\7#\2\2\u0192"+
		"p\3\2\2\2\u0193\u0194\7u\2\2\u0194\u0195\7v\2\2\u0195\u0196\7t\2\2\u0196"+
		"\u0197\7w\2\2\u0197\u0198\7e\2\2\u0198\u0199\7v\2\2\u0199r\3\2\2\2\u019a"+
		"\u019b\7n\2\2\u019b\u019c\7g\2\2\u019c\u019d\7v\2\2\u019dt\3\2\2\2\u019e"+
		"\u019f\7o\2\2\u019f\u01a0\7w\2\2\u01a0\u01a1\7v\2\2\u01a1v\3\2\2\2\u01a2"+
		"\u01a3\7r\2\2\u01a3\u01a4\7t\2\2\u01a4\u01a5\7k\2\2\u01a5\u01a6\7p\2\2"+
		"\u01a6\u01a7\7v\2\2\u01a7\u01a8\7n\2\2\u01a8\u01a9\7p\2\2\u01a9\u01aa"+
		"\7#\2\2\u01aax\3\2\2\2\u01ab\u01ac\7r\2\2\u01ac\u01ad\7t\2\2\u01ad\u01ae"+
		"\7k\2\2\u01ae\u01af\7p\2\2\u01af\u01b0\7v\2\2\u01b0\u01b1\7#\2\2\u01b1"+
		"z\3\2\2\2\u01b2\u01b3\7n\2\2\u01b3\u01b4\7q\2\2\u01b4\u01b5\7q\2\2\u01b5"+
		"\u01b6\7r\2\2\u01b6|\3\2\2\2\u01b7\u01b8\7k\2\2\u01b8\u01b9\7h\2\2\u01b9"+
		"~\3\2\2\2\u01ba\u01bb\7g\2\2\u01bb\u01bc\7n\2\2\u01bc\u01bd\7u\2\2\u01bd"+
		"\u01be\7g\2\2\u01be\u0080\3\2\2\2\u01bf\u01c0\7y\2\2\u01c0\u01c1\7j\2"+
		"\2\u01c1\u01c2\7k\2\2\u01c2\u01c3\7n\2\2\u01c3\u01c4\7g\2\2\u01c4\u0082"+
		"\3\2\2\2\u01c5\u01c6\7o\2\2\u01c6\u01c7\7c\2\2\u01c7\u01c8\7v\2\2\u01c8"+
		"\u01c9\7e\2\2\u01c9\u01ca\7j\2\2\u01ca\u0084\3\2\2\2\u01cb\u01cc\7h\2"+
		"\2\u01cc\u01cd\7q\2\2\u01cd\u01ce\7t\2\2\u01ce\u0086\3\2\2\2\u01cf\u01d0"+
		"\7k\2\2\u01d0\u01d1\7p\2\2\u01d1\u0088\3\2\2\2\u01d2\u01d3\7h\2\2\u01d3"+
		"\u01d4\7p\2\2\u01d4\u008a\3\2\2\2\u01d5\u01d6\7(\2\2\u01d6\u008c\3\2\2"+
		"\2\u01d7\u01d8\7d\2\2\u01d8\u01d9\7t\2\2\u01d9\u01da\7g\2\2\u01da\u01db"+
		"\7c\2\2\u01db\u01dc\7m\2\2\u01dc\u008e\3\2\2\2\u01dd\u01de\7e\2\2\u01de"+
		"\u01df\7q\2\2\u01df\u01e0\7p\2\2\u01e0\u01e1\7v\2\2\u01e1\u01e2\7k\2\2"+
		"\u01e2\u01e3\7p\2\2\u01e3\u01e4\7w\2\2\u01e4\u01e5\7g\2\2\u01e5\u0090"+
		"\3\2\2\2\u01e6\u01e7\7t\2\2\u01e7\u01e8\7g\2\2\u01e8\u01e9\7v\2\2\u01e9"+
		"\u01ea\7w\2\2\u01ea\u01eb\7t\2\2\u01eb\u01ec\7p\2\2\u01ec\u0092\3\2\2"+
		"\2\u01ed\u01ee\7o\2\2\u01ee\u01ef\7q\2\2\u01ef\u01f0\7f\2\2\u01f0\u0094"+
		"\3\2\2\2\u01f1\u01f2\7r\2\2\u01f2\u01f3\7w\2\2\u01f3\u01f4\7d\2\2\u01f4"+
		"\u0096\3\2\2\2\u01f5\u01f7\t\2\2\2\u01f6\u01f5\3\2\2\2\u01f7\u01f8\3\2"+
		"\2\2\u01f8\u01f6\3\2\2\2\u01f8\u01f9\3\2\2\2\u01f9\u0098\3\2\2\2\u01fa"+
		"\u01fc\t\2\2\2\u01fb\u01fa\3\2\2\2\u01fc\u01fd\3\2\2\2\u01fd\u01fb\3\2"+
		"\2\2\u01fd\u01fe\3\2\2\2\u01fe\u01ff\3\2\2\2\u01ff\u0201\7\60\2\2\u0200"+
		"\u0202\t\2\2\2\u0201\u0200\3\2\2\2\u0202\u0203\3\2\2\2\u0203\u0201\3\2"+
		"\2\2\u0203\u0204\3\2\2\2\u0204\u009a\3\2\2\2\u0205\u0209\7$\2\2\u0206"+
		"\u0208\n\3\2\2\u0207\u0206\3\2\2\2\u0208\u020b\3\2\2\2\u0209\u0207\3\2"+
		"\2\2\u0209\u020a\3\2\2\2\u020a\u020c\3\2\2\2\u020b\u0209\3\2\2\2\u020c"+
		"\u020d\7$\2\2\u020d\u009c\3\2\2\2\u020e\u0212\t\4\2\2\u020f\u0211\t\5"+
		"\2\2\u0210\u020f\3\2\2\2\u0211\u0214\3\2\2\2\u0212\u0210\3\2\2\2\u0212"+
		"\u0213\3\2\2\2\u0213\u009e\3\2\2\2\u0214\u0212\3\2\2\2\u0215\u0216\7\61"+
		"\2\2\u0216\u0217\7\61\2\2\u0217\u021b\3\2\2\2\u0218\u021a\n\3\2\2\u0219"+
		"\u0218\3\2\2\2\u021a\u021d\3\2\2\2\u021b\u0219\3\2\2\2\u021b\u021c\3\2"+
		"\2\2\u021c\u021e\3\2\2\2\u021d\u021b\3\2\2\2\u021e\u021f\bP\2\2\u021f"+
		"\u00a0\3\2\2\2\u0220\u0221\7\61\2\2\u0221\u0222\7,\2\2\u0222\u0226\3\2"+
		"\2\2\u0223\u0225\n\6\2\2\u0224\u0223\3\2\2\2\u0225\u0228\3\2\2\2\u0226"+
		"\u0224\3\2\2\2\u0226\u0227\3\2\2\2\u0227\u0229\3\2\2\2\u0228\u0226\3\2"+
		"\2\2\u0229\u022a\7,\2\2\u022a\u022b\7\61\2\2\u022b\u022c\3\2\2\2\u022c"+
		"\u022d\bQ\2\2\u022d\u00a2\3\2\2\2\u022e\u0230\t\7\2\2\u022f\u022e\3\2"+
		"\2\2\u0230\u0231\3\2\2\2\u0231\u022f\3\2\2\2\u0231\u0232\3\2\2\2\u0232"+
		"\u0233\3\2\2\2\u0233\u0234\bR\2\2\u0234\u00a4\3\2\2\2\u0235\u0236\7^\2"+
		"\2\u0236\u0237\t\b\2\2\u0237\u00a6\3\2\2\2\13\2\u01f8\u01fd\u0203\u0209"+
		"\u0212\u021b\u0226\u0231\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}