parser grammar JParser;

options {  
  tokenVocab = JLexer;
} 

start 
  : instrucciones 
;

instrucciones
  : instruccion instrucciones 
  | EOF
;
//---------------------
// GENERAL
//---------------------
instruccion 
  : STRING PTCOMA
;